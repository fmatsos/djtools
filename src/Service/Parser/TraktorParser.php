<?php

namespace Service\Parser;


use Doctrine\Common\Collections\ArrayCollection;
use Entity\Parser\Entry;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class TraktorParser
 * @package AppBundle\Service\Parser
 */
class TraktorParser extends Parser
{
    /**
     * @var array
     */
    const EXTENSIONS = array('nml');


    /**
     * @var Crawler $xmlCrawler
     */
    private $xmlCrawler;


    /**
     * NMLParser constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param File $file
     *
     * @return ArrayCollection|FileException
     */
    public function load(File $file, $extension)
    {
        switch ($extension) {
            case self::TYPE_TRAKTOR_NML:
                $this->collection = $this->parseNmlFile($file);
            break;

            case self::TYPE_M3U:
                $this->collection = $this->container->get('parser.m3u')->parseFile($file);
            break;
        }

        return $this->collection;
    }

    /**
     * @param File $file
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|FileException
     */
    public function parseNmlFile(File $file)
    {
        $collection       = new ArrayCollection();
        $fileObj          = $file->openFile();
        $this->xmlCrawler = new Crawler($fileObj->fread($fileObj->getSize()));

        if ('Traktor' != $this->xmlCrawler->filter('NML > HEAD')->attr('PROGRAM')) {
            return new FileException('File is not a valid Traktor NML');
        }

        $xmlCollection = $this->xmlCrawler->filter('NML > COLLECTION');

        /**
         * @var \DOMElement $entry
         */
        foreach ($xmlCollection->children() as $entry) {
            $entryModel        = new Entry();
            $datas['infos']    = $entry->getElementsByTagName('INFO');
            $datas['location'] = $entry->getElementsByTagName('LOCATION');
            $datas['album']    = $entry->getElementsByTagName('ALBUM');
            $datas['tempo']    = $entry->getElementsByTagName('TEMPO');

            $entryModel->setArtist($this->cleanString($entry->getAttribute('ARTIST')))
                       ->setTrackName($this->cleanString($entry->getAttribute('TITLE')));

            /**
             * @var \DOMNodeList $nodeList
             */
            foreach ($datas as $index => $nodeList) {
                if ($nodeList->length > 0) {
                    $node = $nodeList->item(0);

                    switch($node->nodeName) {
                        case 'INFO':
                            $entryModel->setGenre($this->cleanString($node->getAttribute('GENRE')))
                                       ->setBitrate($node->getAttribute('BITRATE'))
                                       ->setSize($this->cleanString($node->getAttribute('FILESIZE')));
                        break;

                        case 'LOCATION':
                            $drive     = $node->getAttribute('VOLUME');
                            $directory = str_replace(':', '', $node->getAttribute('DIR'));
                            $filename  = $node->getAttribute('FILE');

                            $entryModel->setFilePath($drive.$directory.$filename);
                        break;

                        case 'ALBUM':
                            $entryModel->setTrackNumber($this->cleanString($node->getAttribute('TRACK')))
                                       ->setAlbum($this->cleanString($node->getAttribute('TITLE')));
                        break;

                        case 'TEMPO':
                            $entryModel->setBpm($node->getAttribute('BPM'));
                        break;
                    }
                }
            }

            if (!$collection->contains($entryModel)) {
                $collection->add($entryModel);
            }
        }

        return $collection;
    }
}