<?php

namespace Service\Parser;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class Parser
 * @package AppBundle\Service\Parser
 */
abstract class Parser
{
    const TYPE_M3U = 'm3u';
    
    const TYPE_TRAKTOR_NML = 'nml';

    const TYPE_SERATO_XML  = 'xml';

    const TYPE_SERATO_CSV  = 'csv';

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var ArrayCollection $collection
     */
    protected $collection;


    /**
     * NMLParser constructor.
     */
    public function __construct(Container $container)
    {
        $this->container  = $container;
        $this->collection = new ArrayCollection();
    }

    /**
     * @param File $file
     */
    abstract public function load(File $file, $extension);

    /**
     * @return mixed
     */
    //abstract public function getSetlist();

    /**
     * @return ArrayCollection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @return ArrayCollection
     */
    public function getArtists()
    {
        $artists = new ArrayCollection();

        /**
         * @var Entry $entry
         */
        foreach ($this->collection as $entry) {
            if (!$artists->contains($entry->getArtist())) {
                $artists->add($entry->getArtist());
            }
        }

        return $artists;
    }

    /**
     * @return ArrayCollection
     */
    public function getTracks()
    {
        $tracks = new ArrayCollection();

        /**
         * @var Entry $entry
         */
        foreach ($this->collection as $entry) {
            if (!$tracks->contains($entry->getTrackName())) {
                $tracks->add($entry->getTrackName());
            }
        }

        return $tracks;
    }

    /**
     * @return ArrayCollection
     */
    public function getAlbums()
    {
        $albums = new ArrayCollection();

        /**
         * @var Entry $entry
         */
        foreach ($this->collection as $entry) {
            if (!$albums->contains($entry->getAlbum())) {
                $albums->add($entry->getAlbum());
            }
        }

        return $albums;
    }


    /**
     * @param string $str
     *
     * @return mixed
     */
    public function cleanString($str)
    {
        /** Mise en minuscules (chaîne utf-8 !) */
        $str = mb_strtolower($str, 'utf-8');

        /** Nettoyage des caractères */
        mb_regex_encoding('utf-8');
        $str = trim(preg_replace('/ +/', ' ', mb_ereg_replace('[^0-9\p{L}]+', ' ', $str)));

        /** strtr() sait gérer le multibyte */
        $str = strtr($str, array(
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'a'=>'a', 'a'=>'a', 'a'=>'a', 'ç'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'd'=>'d', 'd'=>'d', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'g'=>'g', 'g'=>'g', 'g'=>'g', 'h'=>'h', 'h'=>'h', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', '?'=>'i', 'j'=>'j', 'k'=>'k', '?'=>'k', 'l'=>'l', 'l'=>'l', 'l'=>'l', '?'=>'l', 'l'=>'l', 'ñ'=>'n', 'n'=>'n', 'n'=>'n', 'n'=>'n', '?'=>'n', '?'=>'n', 'ð'=>'o', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'œ'=>'o', 'ø'=>'o', 'r'=>'r', 'r'=>'r', 's'=>'s', 's'=>'s', 's'=>'s', 'š'=>'s', '?'=>'s', 't'=>'t', 't'=>'t', 't'=>'t', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'w'=>'w', 'ý'=>'y', 'ÿ'=>'y', 'y'=>'y', 'z'=>'z', 'z'=>'z', 'ž'=>'z'
        ));

        return ucwords($str);
    }
}