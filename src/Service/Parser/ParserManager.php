<?php

namespace Service\Parser;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class ParserManager
 * @package AppBundle\Service\Parser
 */
class ParserManager
{
    /**
     * ParserManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container  = $container;
    }


    public function getParserForExtension($extension)
    {
        $parsers = $this->container->getParameter('parsers');

        foreach ($parsers as $index => $parser) {
            if (in_array($extension, $parser['extensions'])) {
                return $this->container->get($parser['service_name']);
            }
        }
    }
}