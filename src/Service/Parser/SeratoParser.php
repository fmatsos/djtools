<?php

namespace Service\Parser;


use AppBundle\Entity\Parser\Entry;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class SeratoParser
 * @package AppBundle\Service\Parser
 */
class SeratoParser extends Parser
{
    /**
     * @var array
     */
    const EXTENSIONS = array('xml', 'csv');


    /**
     * @var Crawler $xmlCrawler
     */
    private $xmlCrawler;


    /**
     * NMLParser constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param File $file
     *
     * @return ArrayCollection|FileException
     */
    public function load(File $file, $extension)
    {
        switch ($file->getExtension()) {
            case self::TYPE_SERATO_XML:
                //$this->collection = $this->parseNmlFile($file);
            break;

            case self::TYPE_SERATO_CSV:
                //$this->collection = $this->parseNmlFile($file);
            break;

            case self::TYPE_M3U:
                $this->collection = $this->container->get('parser.m3u')->parseFile($file);
            break;
        }

        return $this->collection;
    }

}