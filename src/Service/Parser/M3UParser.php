<?php

namespace Service\Parser;


use AppBundle\Entity\Parser\Entry;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class M3UParser
 * @package AppBundle\Service\Parser
 */
class M3UParser extends Parser
{
    /**
     * @var array
     */
    const EXTENSIONS = array('m3u', 'm3u8');


    /**
     * @var Crawler $xmlCrawler
     */
    private $xmlCrawler;


    /**
     * NMLParser constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param File $file
     *
     * @return ArrayCollection|FileException
     */
    public function load(File $file)
    {
        switch ($file->getExtension()) {
            case self::TYPE_SERATO_XML:
                //$this->collection = $this->parseNmlFile($file);
            break;

            case self::TYPE_SERATO_CSV:
                //$this->collection = $this->parseNmlFile($file);
            break;

            case self::TYPE_M3U:
                $this->collection = $this->container->get('parser.m3u')->parseFile($file);
            break;
        }

        return $this->collection;
    }

}