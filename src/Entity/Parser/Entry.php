<?php

namespace Entity\Parser;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Entry
 * @package Entity\Parser
 *
 * @ORM\Entity
 * @ORM\Table(name="parser_entry")
 */
class Entry
{
    /**
     * @var int id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $artist
     *
     * @ORM\Column(name="artist", type="string")
     */
    private $artist;

    /**
     * @var string $trackName
     *
     * @ORM\Column(name="track_name", type="string")
     */
    private $trackName;

    /**
     * @var integer $trackNumber
     *
     * @ORM\Column(name="track_number", type="smallint")
     */
    private $trackNumber;

    /**
     * @var string $album
     *
     * @ORM\Column(name="album", type="string")
     */
    private $album;

    /**
     * @var string $genre
     *
     * @ORM\Column(name="artist", type="string")
     */
    private $genre;

    /**
     * @var integer $bpm
     *
     * @ORM\Column(name="bpm", type="smallint")
     */
    private $bpm;

    /**
     * @var integer $bitrate
     *
     * @ORM\Column(name="bitrate", type="smallint")
     */
    private $bitrate;

    /**
     * @var integer $size
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var integer $setlistPosition
     *
     * @ORM\Column(name="setlist_position", type="smallint")
     */
    private $setlistPosition;

    /**
     * @var string $filePath
     *
     * @ORM\Column(name="file_path", type="string")
     */
    private $filePath;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param $artist
     *
     * @return $this
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackName()
    {
        return $this->trackName;
    }

    /**
     * @param $trackName
     *
     * @return $this
     */
    public function setTrackName($trackName)
    {
        $this->trackName = $trackName;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrackNumber()
    {
        return $this->trackNumber;
    }

    /**
     * @param $trackNumber
     *
     * @return $this
     */
    public function setTrackNumber($trackNumber)
    {
        $this->trackNumber = $trackNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param $album
     *
     * @return $this
     */
    public function setAlbum($album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param $genre
     *
     * @return $this
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return int
     */
    public function getBpm()
    {
        return $this->bpm;
    }

    /**
     * @param int $bpm
     *
     * @return Entry
     */
    public function setBpm($bpm)
    {
        $this->bpm = $bpm;

        return $this;
    }

    /**
     * @return int
     */
    public function getBitrate()
    {
        return $this->bitrate;
    }

    /**
     * @param $bitrate
     *
     * @return $this
     */
    public function setBitrate($bitrate)
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return int
     */
    public function getSetlistPosition()
    {
        return $this->setlistPosition;
    }

    /**
     * @param $setlistPosition
     *
     * @return $this
     */
    public function setSetlistPosition($setlistPosition)
    {
        $this->setlistPosition = $setlistPosition;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     *
     * @return Entry
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }
}