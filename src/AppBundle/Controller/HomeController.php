<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package AppBundle\Controller
 *
 */
class HomeController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/", name="home_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:Home:index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @Route("/contact", name="home_contact")
     */
    public function contactAction(Request $request)
    {

    }
}