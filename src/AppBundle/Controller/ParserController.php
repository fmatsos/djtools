<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ParserController
 * @package AppBundle\Controller
 *
 * @Route("/parser")
 */
class ParserController extends Controller
{
    /**
     * @Route("/", name="parser_index")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
                     ->add('playlist', FileType::class)
                     ->getForm()
                     ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            /**
             * @var UploadedFile $fileUploaded
             */
            $fileUploaded = $form->get('playlist')->getData();

            $extension = $fileUploaded->getClientOriginalExtension();
            $file      = $fileUploaded->move($this->getParameter('upload_playlists_dir'), md5(uniqid()).'.'.$extension);

            //$fileName = $this->get('parser.uploader')->upload($file);

            $serializer = new Serializer(array(new ObjectNormalizer()), array(new JsonEncoder()));
            $json       = array();
            $parser     = $this->get('parser.manager')->getParserForExtension($extension);
            $datas      = $parser->load($file, $extension);

            foreach ($datas as $entry) {
                $json[] = json_decode($serializer->serialize($entry, 'json'));
            }

            return $this->render('AppBundle:Parser:result.html.twig', array('datas' => $json));
        }

        return $this->render('AppBundle:Parser:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
